import Data.List ( nub )

main :: IO ()
main = do
    input <- getLine
    let origLength :: Int
        origLength = length input
    let setLength :: Int
        setLength = length $ nub input
    print $ solve origLength setLength

solve :: Int -> Int -> Int
solve everything unique
    | everything == unique = 1
    | otherwise = 0