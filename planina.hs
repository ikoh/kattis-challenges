input :: String -> Int
input raw = read raw :: Int

solve :: Int -> Int -> Int
solve accumVal 0 = accumVal ^ 2
solve accumVal iter = solve (accumVal * 2 - 1) (iter - 1)

main :: IO ()
main = interact $ show . solve 2 . input