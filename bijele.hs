str2Int :: String -> Int
str2Int a = read a :: Int

input :: String -> [Int]
input a = map str2Int $ words a

solve :: [Int] -> [Int]
solve = zipWith (-) [1, 1, 2, 2, 2, 8]

output :: [Int] -> String
output a = unwords $ map show a

main :: IO ()
main = interact $ output . solve . input