getDimensions :: String -> [Int]
getDimensions input = map (\x -> read x :: Int) $ words input

calculateCutDims :: [Int] -> [(Int, Int)]
calculateCutDims inputList = [(inputList !! 1, horizontalRem), (last inputList, verticalRem)]
    where
        fullSide = head inputList
        horizontalRem = fullSide - (inputList !! 1)
        verticalRem = fullSide - last inputList

getLargerCut :: (Int, Int) -> Int
getLargerCut = uncurry max

getMaxVol :: [(Int, Int)] -> Int
getMaxVol cutDims = product (map getLargerCut cutDims) * 4

solve :: String -> String
solve = show . getMaxVol . calculateCutDims . getDimensions

main :: IO ()
main = do
    input <- getLine
    putStrLn $ solve input