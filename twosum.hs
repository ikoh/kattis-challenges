main :: IO ()
main = do
    input <- getLine
    print . sum . map (\x -> read x :: Int) $ words input