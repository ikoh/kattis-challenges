input :: String -> [Int]
input rawInput = map string2Int $ lines rawInput

string2Int :: String -> Int
string2Int rawElem = read rawElem :: Int

signFinder :: [Int] -> [Int]
signFinder inputList = [signum (head inputList), signum (last inputList)]

solve :: [Int] -> String
solve [1, 1] = "1"
solve [-1, 1] = "2"
solve [-1, -1] = "3"
solve [1, -1] = "4"

main :: IO ()
main = interact $ solve . signFinder . input