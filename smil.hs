import Data.List (zip4, sort)

main :: IO ()
main = do
    input <- getLine
    let twoCharRef = [":)", ";)"]
        threeCharRef = [":-)", ";-)"]
        twoCharSolve = solve (twoCharSmile input) twoCharRef
        threeCharSolve = solve (threeCharSmile input) threeCharRef
    putStrLn . unlines . map show $ sort (twoCharSolve ++ threeCharSolve)

threeZip :: String -> [(Char, Char, Int)]
threeZip inputStr = zip3 inputStr (tail inputStr) [0..]

fourZip :: String -> [(Char, Char, Char, Int)]
fourZip inputStr = zip4 inputStr (tail inputStr) (tail $ tail inputStr) [0..]

twoCharSmile :: String -> [(String, Int)]
twoCharSmile inputStr = map (\(a, b, c) -> ([a, b], c)) twoCharZipped
  where
    twoCharZipped = threeZip inputStr

threeCharSmile :: String -> [(String, Int)]
threeCharSmile inputStr = map (\(a, b, c, d) -> ([a, b, c], d)) threeCharZipped
  where
    threeCharZipped = fourZip inputStr

solve :: [(String, Int)] -> [String] -> [Int]
solve zippedInput refList = map snd smiles
  where
    smiles = filter (\(a, b) -> a `elem` refList) zippedInput