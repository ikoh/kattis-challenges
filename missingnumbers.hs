import Data.List

str2Int :: String -> Int
str2Int a = read a :: Int

input :: String -> [Int]
input a = map str2Int $ lines a

solve :: String -> String
solve a
    | length intended == length observed = "good job"
    | otherwise = unlines $ map show $ intended \\ observed
    where
        intended = [1 .. (last $ input a)]
        observed = tail $ input a

main :: IO ()
main = interact solve