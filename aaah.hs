main :: IO ()
main = do
    input <- getContents
    putStrLn . goNoGo $ lines input

goNoGo :: [String] -> String
goNoGo xs
    | length (head xs) >= length (last xs) = "go"
    | otherwise = "no"