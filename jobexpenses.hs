main :: IO ()
main = do
    input <- getContents
    let preppedInput = map (\x -> read x :: Int) $ words $ tail input
    let expenses = sum (filter (< 0) preppedInput) * (-1)
    print expenses