main :: IO ()
main = do
    input <- getContents
    let linedInput = lines input
        numCases = read $ head linedInput :: Int
        textList = tail linedInput
    putStrLn . unlines $ zipWith (++) (map descriptionGen $ [1 .. numCases]) (map latin2T9 textList)

char2Keypress :: Char -> [Char]
char2Keypress ' ' = "0"
char2Keypress 'a' = "2"
char2Keypress 'b' = "22"
char2Keypress 'c' = "222"
char2Keypress 'd' = "3"
char2Keypress 'e' = "33"
char2Keypress 'f' = "333"
char2Keypress 'g' = "4"
char2Keypress 'h' = "44"
char2Keypress 'i' = "444"
char2Keypress 'j' = "5"
char2Keypress 'k' = "55"
char2Keypress 'l' = "555"
char2Keypress 'm' = "6"
char2Keypress 'n' = "66"
char2Keypress 'o' = "666"
char2Keypress 'p' = "7"
char2Keypress 'q' = "77"
char2Keypress 'r' = "777"
char2Keypress 's' = "7777"
char2Keypress 't' = "8"
char2Keypress 'u' = "88"
char2Keypress 'v' = "888"
char2Keypress 'w' = "9"
char2Keypress 'x' = "99"
char2Keypress 'y' = "999"
char2Keypress 'z' = "9999"

withPauses :: [String] -> [String]
withPauses [] = []
withPauses (x:xs)
    | null xs = [x]
    | head x == head (head xs) = x : " " : withPauses xs
    | otherwise = x : withPauses xs

latin2T9 :: String -> String
latin2T9 = mconcat . withPauses . map char2Keypress

descriptionGen :: Int -> String
descriptionGen x = "Case #" ++ show x ++ ": "