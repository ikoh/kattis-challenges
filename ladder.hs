main :: IO ()
main = do
    input <- getLine
    print . solve . parse $ words input

parse :: [String] -> (Float, Float)
parse xs = (read $ head xs, read $ last xs)

solve :: (Float, Float) -> Integer
solve (a, b)
    = ceiling
    $ a / (sin $ b * 0.01745329252)
