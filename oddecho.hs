main :: IO ()
main = do
    input <- getContents
    let parsedInput = tail $ lines input :: [String]
        numberedInput = zip parsedInput [1..] :: [(String, Int)]
        oddNumberedInput = filter (\(a, b) -> odd b) numberedInput :: [(String, Int)]
        inputToReturn = map fst oddNumberedInput :: [String]
    putStrLn $ unlines inputToReturn