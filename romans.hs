main :: IO ()
main = do
    input <- getLine
    let preppedInput = read input :: Double
    print . round $ (preppedInput * 1000 * 5280) / 4854