solve :: String -> String
solve raw = show $ length $ filter (< 0) $ map (\x -> read x :: Int) $ words $ last $ lines raw

main :: IO ()
main = do
    input <- getContents
    putStrLn $ solve input  