getStops :: String -> [Int]
getStops rawInput =
    map (\x -> read x :: Int)
    $ tail
    $ lines rawInput

buildCouplet :: Int -> (Int, Int)
buildCouplet stops = (stops, 0)

prepForLogic :: String -> [(Int, Int)]
prepForLogic rawInput = map buildCouplet $ getStops rawInput

paxCount :: (Int, Int) -> Int
paxCount (0, pax) = pax
paxCount (stops, pax) = paxCount (stops - 1, pax * 2 + 1)

solve :: String -> String
solve rawInput =
    unlines
    $ map (show . paxCount)
    $ prepForLogic rawInput

main :: IO ()
main = do
    input <- getContents
    putStrLn $ solve input