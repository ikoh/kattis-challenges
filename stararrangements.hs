import Data.List ( foldl' )


-- Logic to build all possible star configurations

buildSequence :: Int -> [Int]
buildSequence n
    | even n = [2 .. (div n 2)]
    | otherwise = [2 .. (div n 2 + 1)]

listlet :: Int -> [[Int]]
listlet element = [[element, element - 1], [element, element]]

buildStarConfig :: Int -> [[Int]]
buildStarConfig n = 
    foldl' (++) []
    $ map listlet
    $ buildSequence n


-- Logic to identify valid star configurations

checkValidity :: Int -> [Int] -> Bool
checkValidity myInputInt myListlet = 
    elem myInputInt 
    $ takeWhile (<= myInputInt)
    $ scanl (+) 0
    $ cycle myListlet

getValidConfigs :: Int -> [[Int]]
getValidConfigs myInputInt = 
    filter (checkValidity myInputInt)
    $ buildStarConfig myInputInt


-- Get valid star configurations and prepare for outputting

toString :: [Int] -> String
toString myListlet = mconcat [show $ head myListlet, ",", show $ last myListlet]

solve :: Int -> [String]
solve myInputInt = header : validConfigs
    where
        header = show myInputInt ++ ":"
        validConfigs = map toString $ getValidConfigs myInputInt


main :: IO ()
main = do
    input <- getLine
    let inputInt = read input :: Int
    putStrLn . unlines $ solve inputInt