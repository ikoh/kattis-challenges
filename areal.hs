solve :: String -> String
solve input = show $ 4 * sqrt area
    where area = read input :: Double

main :: IO ()
main = do
    input <- getLine
    putStrLn $ solve input