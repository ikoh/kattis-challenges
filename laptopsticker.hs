main :: IO ()
main = do
    raw <- getLine
    let input = listToTuple . map read $ words raw :: (Int, Int, Int, Int)
    print $ solve input

listToTuple :: [Int] -> (Int, Int, Int, Int)
listToTuple xs
    | length xs /= 4 = error "List must be of length 4"
    | otherwise      = (head xs, xs !! 1, xs !! 2, last xs)

solve :: (Int, Int, Int, Int) -> Int
solve (wc, hc, ws, hs)
    | wc - ws >= 2 && hc - hs >= 2 = 1
    | otherwise = 0