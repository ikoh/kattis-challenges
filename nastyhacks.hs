expectedResult :: String -> Ordering
expectedResult rawElem = compare noAdvertRev netAdvertRev
    where
        toDouble = map (\x -> read x :: Double) $ words rawElem
        noAdvertRev = head toDouble
        expectations = tail toDouble
        netAdvertRev = head expectations - last expectations

decider :: Ordering -> String
decider LT = "advertise"
decider GT = "do not advertise"
decider EQ = "does not matter"

advertisingLogic :: String -> String
advertisingLogic = decider . expectedResult

main :: IO ()
main = do
    input <- getContents
    putStrLn $ unlines $ map advertisingLogic $ tail $ lines input