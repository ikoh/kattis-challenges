import qualified Data.Text as T

main :: IO ()
main = do
    input <- getContents
    let parsed = map T.toLower . tail . T.lines $ T.pack input
        sessions = length $ filter canAttend parsed
    putStrLn $ outcome sessions

canAttend :: T.Text -> Bool
canAttend label
    | T.isInfixOf (T.pack "pink") label = True
    | T.isInfixOf (T.pack "rose") label = True
    | otherwise                         = False

outcome :: Int -> String
outcome 0 = "I must watch Star Wars with my daughter"
outcome x = show x