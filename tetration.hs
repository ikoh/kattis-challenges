toDouble :: String -> Double
toDouble x = read x :: Double

solve :: Double -> Double
solve x = x ** (1 / x)

main :: IO ()
main = do
    input <- getLine
    putStrLn . show . solve $ toDouble input

-- Note that because 1/e <= N <= e, the super-root of N is well defined and can
-- be extended to n == infinity. Obtaining the super-root therefore follows the
-- function N ** (1 / N), where (**) is the operator for real-valued exponents;
-- more information can be found in the Wikipedia article on tetration 