maxAppliances :: [Int] -> Int
maxAppliances [] = 1
maxAppliances strips = 1 + sum (map (subtract 1) strips)

buildListlets :: String -> [Int]
buildListlets = map (\x -> read x :: Int) . tail . words

getRelevantInfo :: String -> [String]
getRelevantInfo = tail . lines

solve :: String -> [String]
solve input = map (show . maxAppliances . buildListlets) $ getRelevantInfo input

main :: IO ()
main = do
    input <- getContents
    putStrLn . unlines $ solve input