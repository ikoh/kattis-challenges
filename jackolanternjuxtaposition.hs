import Data.List

string2Int :: String -> Int
string2Int rawElem = read rawElem :: Int

solve :: String -> String
solve raw = show $ foldl' (*) 1 $ map string2Int $ words raw

main :: IO ()
main = interact solve