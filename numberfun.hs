main :: IO ()
main = do
    input <- getContents
    let preppedInput :: [String]
        preppedInput = tail $ lines input
    putStrLn . unlines $ map (decider . checkOps . getInts) preppedInput

getInts :: String -> [Double]
getInts stringInts 
    = map (\x -> read x :: Double) 
    $ words stringInts

checkOps :: [Double] -> Bool
checkOps [x, y, z] = z `elem` doMaths
    where doMaths = [x + y, abs (x - y), x * y, x / y, y / x]

decider :: Bool -> String
decider True  = "Possible"
decider False = "Impossible"