import Data.Char (toLower)

main :: IO ()
main = do
    input <- getLine
    let translation = map (charTrans . toLower) input
    putStrLn $ mconcat translation

charTrans :: Char -> String
charTrans 'a' = "@"
charTrans 'b' = "8"
charTrans 'c' = "("
charTrans 'd' = "|)"
charTrans 'e' = "3"
charTrans 'f' = "#"
charTrans 'g' = "6"
charTrans 'h' = "[-]"
charTrans 'i' = "|"
charTrans 'j' = "_|"
charTrans 'k' = "|<"
charTrans 'l' = "1"
charTrans 'm' = "[]\\/[]"
charTrans 'n' = "[]\\[]"
charTrans 'o' = "0"
charTrans 'p' = "|D"
charTrans 'q' = "(,)"
charTrans 'r' = "|Z"
charTrans 's' = "$"
charTrans 't' = "']['"
charTrans 'u' = "|_|"
charTrans 'v' = "\\/"
charTrans 'w' = "\\/\\/"
charTrans 'x' = "}{"
charTrans 'y' = "`/"
charTrans 'z' = "2"
charTrans x   = [x]