import Data.List ( sort )

main :: IO ()
main = do
    input <- getContents
    let preppedInput = preprocess input
    let minDist = solve preppedInput
    putStrLn . unlines $ map show minDist

preprocess :: String -> [[String]]
preprocess = map words . tail . lines

getRelevantCases :: [[String]] -> [[String]]
getRelevantCases strings = map fst $ filter (even . snd) numberedStrings
    where numberedStrings = zip strings [1..]

getUsableValues :: [[String]] -> [[Int]]
getUsableValues = map toInt
    where toInt = map (\x -> read x :: Int)

distanceWalked :: [Int] -> Int
distanceWalked locations = oneWayDist * 2
    where 
        sortedLocations = sort locations
        oneWayDist = sum $ zipWith (-) (tail sortedLocations) (init sortedLocations)

solve :: [[String]] -> [Int]
solve strings = map distanceWalked usableValues
    where 
        usableValues = getUsableValues $ getRelevantCases strings