import Data.List (nub)

main :: IO ()
main = do
    input <- getContents
    let preppedInput :: [[String]]
        preppedInput = map words $ tail $ lines input
    let days :: [[Int]]
        days = map listBuilder preppedInput
    print . length . nub $ mconcat days

listBuilder :: [String] -> [Int]
listBuilder eventDates = [start .. end]
    where
        start = read (head eventDates) :: Int
        end = read (last eventDates) :: Int