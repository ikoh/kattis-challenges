main :: IO ()
main = do
    rawInput <- getContents
    let input = map read $ words rawInput :: [Int]
    let answer = sum (tail input) - (head input - 1)
    print answer
