import Data.List (nub)

main :: IO ()
main = do
    input <- getContents
    let preppedInput = map (\x -> read x :: Int) $ lines input
    print . length . nub $ map (`mod` 42) preppedInput