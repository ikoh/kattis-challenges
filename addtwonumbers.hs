main :: IO ()
main = do
    input <- getLine
    let preppedInput = map (\x -> read x :: Integer) $ words input
    print $ sum preppedInput