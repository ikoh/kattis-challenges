module Main where

main :: IO ()
main = do
    input <- getLine
    let inputDouble = read input :: Double
    print (inputDouble / 4.0)