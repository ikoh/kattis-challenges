import Data.List (sort, group, nub)

main :: IO ()
main = do
    input <- getLine
    let wordLength = length input `div` 3
    putStrLn . solve $ parse input wordLength

(|>) :: t1 -> (t1 -> t2) -> t2
(|>) f g = g f

parse :: String -> Int -> [String]
parse [] _ = []
parse str lengte = take lengte str : parse (drop lengte str) lengte

solve :: [String] -> String
solve xs =
    sort xs |> group |>
    filter (\x -> length x > 1) |>
    head |> nub |> head