main :: IO ()
main = do
    input <- getContents
    let presses = read . head $ lines input :: Int
    let pressTimes = map (\x -> read x :: Int) . tail $ lines input
    putStrLn $ solve presses pressTimes

solve :: Int -> [Int] -> String
solve presses pressTimes
    | odd presses = "still running"
    | otherwise = show . sum $ displayedTime pressTimes

displayedTime :: [Int] -> [Int]
displayedTime [] = []
displayedTime xs = last pair - head pair : displayedTime (drop 2 xs)
    where
    pair = take 2 xs