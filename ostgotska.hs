import qualified Data.Text as T

main :: IO ()
main = do
    input <- getLine
    let ingestedInput = ingest input
    putStrLn $ categorise ingestedInput

ingest :: String -> [T.Text]
ingest rawInput = T.words $ T.pack rawInput

checkAE :: T.Text -> Bool
checkAE = T.isInfixOf ae
    where ae = T.pack "ae"

countAE :: [T.Text] -> Double
countAE sentence = fromIntegral . length $ filter checkAE sentence

categorise :: [T.Text] -> String
categorise sentence
    | aeLength / origLength >= 0.4 = "dae ae ju traeligt va"
    | otherwise = "haer talar vi rikssvenska"
    where
        origLength = fromIntegral $ length sentence
        aeLength = countAE sentence