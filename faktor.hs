main :: IO ()
main = do
    input <- getLine
    let usableInput = map (\x -> read x :: Int) $ words input
    print (head usableInput * last usableInput - (head usableInput - 1))