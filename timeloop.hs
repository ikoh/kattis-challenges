listBuilder :: Int -> [String]
listBuilder 0 = []
listBuilder n = map show [1..n]

abracadabraBuilder :: Int -> [String]
abracadabraBuilder 0 = []
abracadabraBuilder n = replicate n "Abracadabra"

oneLine :: String -> String -> String
oneLine [] _ = []
oneLine _ [] = []
oneLine a b = mconcat [a, " ", b]

loopBreaker :: Int -> String
loopBreaker n = unlines $ zipWith oneLine (listBuilder n) (abracadabraBuilder n)

main :: IO ()
main = interact $ loopBreaker . read
