main :: IO ()
main = do
    input <- getLine
    let preppedInput :: [Int]
        preppedInput = map (\x -> read x :: Int) $ words input
    putStrLn $ points preppedInput

points :: [Int] -> String
points [0, 0] = "Not a moose"
points tines
    | head tines == last tines = mconcat ["Even ", show (sum tines)]
    | otherwise = mconcat ["Odd ", show $ maximum tines * 2]