import Numeric ( showFFloat )

data CharGroup
    = Whitespace
    | Lowercase
    | Uppercase
    | Symbol
    deriving (Eq, Show)

main :: IO ()
main = do
    input <- getLine
    let lineLength :: Double
        lineLength = fromIntegral $ length input
    let processedInput :: [CharGroup]
        processedInput = map charClassifier input
    putStrLn . unlines $ getSummaryStats lineLength processedInput

charClassifier :: Char -> CharGroup
charClassifier char
    | char == '_'              = Whitespace
    | char `elem` ['a' .. 'z'] = Lowercase
    | char `elem` ['A' .. 'Z'] = Uppercase
    | otherwise                = Symbol

getCharTypeLength :: CharGroup -> [CharGroup] -> Double
getCharTypeLength x xs = fromIntegral . length $ filter (== x) xs

showFullPrecision :: Double -> String
showFullPrecision x = showFFloat Nothing x ""

getSummaryStats :: Double -> [CharGroup] -> [String]
getSummaryStats charLength processedChars = map (showFullPrecision . (/ charLength)) charGroupCounts
    where
        numWhitespace = getCharTypeLength Whitespace processedChars
        numLowercase  = getCharTypeLength Lowercase processedChars
        numUppercase  = getCharTypeLength Uppercase processedChars
        numSymbols    = charLength - sum [numWhitespace, numLowercase, numUppercase]
        charGroupCounts = [numWhitespace, numLowercase, numUppercase, numSymbols]