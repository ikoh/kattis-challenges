main :: IO ()
main = do
    input <- getContents
    let preppedInput = map (\x -> read x :: Int) $ lines input
    let absDiff = abs (head preppedInput - last preppedInput)
    let signedDiff = head preppedInput - last preppedInput
    let absDiffCat = absDiffCategoriser absDiff
    let magnitude = magnitudeFinder absDiffCat absDiff
    let direction = directionFinder absDiffCat (signum signedDiff)
    print $ magnitude * direction

data DiffCategory = LessThan180 | GreaterThan180 | Is180
    deriving (Eq, Show)

absDiffCategoriser :: Int -> DiffCategory
absDiffCategoriser absdiff
    | absdiff < 180  = LessThan180
    | absdiff > 180  = GreaterThan180
    | absdiff == 180 = Is180

directionFinder :: DiffCategory -> Int -> Int
directionFinder LessThan180 0       = 0
directionFinder LessThan180 (-1)    = 1
directionFinder LessThan180 1       = -1
directionFinder GreaterThan180 (-1) = -1
directionFinder GreaterThan180 1    = 1
directionFinder Is180 _             = 1

magnitudeFinder :: DiffCategory -> Int -> Int
magnitudeFinder Is180 _                = 180
magnitudeFinder LessThan180 absdiff    = absdiff
magnitudeFinder GreaterThan180 absdiff = 360 - absdiff