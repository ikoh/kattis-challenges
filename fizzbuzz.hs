main :: IO ()
main = do
    input <- getLine
    let processedInput = map read $ words input
        fullList = [1 .. last processedInput]
        fizzNum = head processedInput
        buzzNum = head $ tail processedInput
    putStrLn . unlines . reverse $ fizzbuzz fullList fizzNum buzzNum []

fizzbuzz :: [Int] -> Int -> Int -> [String] -> [String]
fizzbuzz [] _ _ initEmpty = initEmpty
fizzbuzz list fizzer buzzer initEmpty
    | head list `mod` buzzer == 0 && head list `mod` fizzer == 0 = fizzbuzz (tail list) fizzer buzzer ("FizzBuzz" : initEmpty)
    | head list `mod` fizzer == 0 && head list `mod` buzzer /= 0 = fizzbuzz (tail list) fizzer buzzer ("Fizz" : initEmpty)
    | head list `mod` buzzer == 0 && head list `mod` fizzer /= 0 = fizzbuzz (tail list) fizzer buzzer ("Buzz" : initEmpty)
    | otherwise = fizzbuzz (tail list) fizzer buzzer (show (head list) : initEmpty)