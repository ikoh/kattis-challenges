main :: IO ()
main = do
    input <- getLine
    let processedInput = words input
    putStrLn $ dupCheck processedInput []

dupCheck :: [String] -> [String] -> String
dupCheck [] ref = "yes"
dupCheck orig ref
    | elem (head orig) ref = "no"
    | otherwise = dupCheck (tail orig) (head orig : ref)