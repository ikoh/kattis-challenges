input :: String -> Int
input raw = read raw :: Int

solve :: Int -> String
solve n
    | odd n = "Alice"
    | otherwise = "Bob"

main :: IO ()
main = interact $ solve . input