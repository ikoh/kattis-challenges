main :: IO ()
main = do
    raw <- getLine
    let input = map read $ words raw :: [Int]
    print $ comparer input

comparer :: [Int] -> Int
comparer xs
    | head xs > last xs = 1
    | otherwise         = 0