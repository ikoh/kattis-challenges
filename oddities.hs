main :: IO ()
main = do
    input <- getContents
    let preppedInput = preprocess input
    putStrLn . unlines $ map classify preppedInput

preprocess :: String -> [Int]
preprocess = map (\x -> read x :: Int) . tail . lines

classify :: Int -> String
classify number
    | even number = show number ++ " is even"
    | otherwise = show number ++ " is odd"