import Data.List

findMaxLimit :: String -> Int
findMaxLimit targetWord = uniqueLength + 10
    where uniqueLength = length $ nub targetWord

solve :: [String] -> String
solve rawList
    | (latestOccurrence + 1) >= guessLimit = "LOSE"
    | otherwise = "WIN"
    where
        targetWord = head rawList
        guessLimit = findMaxLimit targetWord
        guesses = last rawList
        latestOccurrence = maximum $ findIndices (`elem` targetWord) guesses

main :: IO ()
main = do
    input <- getContents
    putStrLn $ solve $ lines input