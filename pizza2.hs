main :: IO ()
main = do
    input <- getLine
    let preppedInput :: [Double]
        preppedInput = map (\x -> read x :: Double) $ words input
    let innerCircle :: Double
        innerCircle = pi * (head preppedInput - last preppedInput) ** 2
    let fullCircle :: Double
        fullCircle = pi * head preppedInput ** 2
    print $ (innerCircle / fullCircle) * 100