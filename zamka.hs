toInt :: [String] -> [Int]
toInt = map (\x -> read x :: Int)

processInput :: String -> [Int]
processInput rawInput = toInt $ lines rawInput

buildSequence :: [Int] -> [Int]
buildSequence input = [lowerBound .. upperBound]
    where
        lowerBound = head input
        upperBound = input !! 1

augmenter :: Int -> (Int, Int)
augmenter int = (int, sum $ toInt $ splitter $ show int)

splitter :: String -> [String]
splitter [] = []
splitter str = [head str] : splitter (tail str)

identifyMatch :: Int -> (Int, Int) -> Bool
identifyMatch reference tup = snd tup == reference

prepForMatching :: [Int] -> ([(Int, Int)], Int)
prepForMatching processed = (map augmenter $ buildSequence processed, last processed)

getMatches :: ([(Int, Int)], Int) -> [Int]
getMatches bigTuple = map fst $ filter (identifyMatch reference) constructedSeq
    where
        reference = snd bigTuple
        constructedSeq = fst bigTuple

solve :: [Int] -> [String]
solve matchesList = map show [minimum matchesList, maximum matchesList]

main :: IO ()
main = do
    input <- getContents
    putStrLn $ unlines $ solve $ getMatches $ prepForMatching $ processInput input