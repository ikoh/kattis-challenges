-- Goal is to model the problem using types, based on original solution

import Data.Char (digitToInt)

-- These are for evenDoubler
newtype SplitRawNum = SplitRawNum Int
newtype SplitRawNumDoubled = SplitRawNumDoubled Int
newtype ReversedIndex = ReversedIndex Int

-- These are for luhnAdder

main :: IO ()
main = do
    undefined