-- First element is X-junction; second element is Y-switch

toTuple :: String -> (Int, Int)
toTuple input = (read firstElem :: Int, read secondElem :: Int)
    where
        makeList = words input
        firstElem = head makeList
        secondElem = last makeList

solve :: (Int, Int) -> String
solve (_, y)
    | even y = "possible"
    | otherwise = "impossible"

main :: IO ()
main = do
    input <- getLine
    putStrLn $ solve $ toTuple input