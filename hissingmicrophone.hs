module Main where

import Data.List ( isInfixOf )

main :: IO ()
main = do
    input <- getLine
    putStrLn $ ssDetector input

ssDetector :: String -> String
ssDetector input
    | "ss" `isInfixOf` input = "hiss"
    | otherwise              = "no hiss"