main :: IO ()
main = do
    input <- getContents
    putStrLn . format . solve $ parse input

parse :: String -> [[Int]]
parse = map ((init . map read) . words) . tail . lines

-- Returns [(observed, previous)]
binder :: [Int] -> [(Int, Int)]
binder xs = zip xs (1 : init xs)

importedTurtles :: (Int, Int) -> Int
importedTurtles (obs, prev)
    | obs > (2 * prev) = obs - 2 * prev
    | otherwise = 0

solve :: [[Int]] -> [Int]
solve = map (sum . map importedTurtles . binder)

-- Why can't this be eta reduced?
format :: [Int] -> String
format xs = unlines $ map show xs