main :: IO ()
main = do
    input <- getLine
    putStrLn . solve $ parse input

parse :: String -> [Int]
parse input = map read $ words input

totalSum :: [Int] -> Int
totalSum drawnCards = sum $ zipWith (*) drawnCards [3, 2, 1]

buyingPossibilities :: Int -> String
buyingPossibilities summedVal
    | summedVal >= 8 = "Province or Gold"
    | summedVal >= 6 && summedVal < 8 = "Duchy or Gold"
    | summedVal == 5 = "Duchy or Silver"
    | summedVal >= 3 && summedVal < 5 = "Estate or Silver"
    | summedVal == 2 = "Estate or Copper"
    | otherwise = "Copper"

solve :: [Int] -> String
solve = buyingPossibilities . totalSum