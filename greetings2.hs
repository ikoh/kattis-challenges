eMultiplier :: String -> String
eMultiplier raw = mconcat $ replicate 2 $ init $ tail raw

greetingBuilder :: String -> String
greetingBuilder eMult = mconcat ["h", eMult, "y"]

main :: IO ()
main = do
    input <- getLine
    putStrLn $ greetingBuilder . eMultiplier $ input