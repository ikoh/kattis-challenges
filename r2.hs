solve :: String -> String
solve input = show $ (2 * s) - r1
    where
        toStringList = map (\x -> read x :: Int) $ words input
        r1 = head toStringList
        s = last toStringList

main :: IO ()
main = do
    input <- getLine
    putStrLn $ solve input