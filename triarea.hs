main :: IO ()
main = do
    raw <- getLine
    let input = map read $ words raw :: [Double]
    print $ product (input ++ [0.5])