import Data.List (nub, group, sort, (\\), sortBy)
import Data.Ord  (comparing)

data Suit
    = P | K | H | T
    deriving (Eq, Ord, Show)

main :: IO ()
main = do
    input <- getLine
    putStrLn . solve $ parse input

parse :: String -> [String]
parse [] = []
parse inputStr = take 3 inputStr : parse (drop 3 inputStr)

charToSuit :: Char -> Suit
charToSuit 'H' = H
charToSuit 'K' = K
charToSuit 'P' = P
charToSuit 'T' = T

basicSolve :: [String] -> [(Int, Suit)]
basicSolve parsedInput = zip (map (13 -) countSuits) (map (charToSuit . head) groupSuits)
  where
    suitsOnly = map head parsedInput :: String
    groupSuits = group $ sort suitsOnly :: [String]
    countSuits = map length groupSuits :: [Int]

imputer :: [(Int, Suit)] -> [(Int, Suit)]
imputer suitInput
    | length suitInput == 4 = suitInput
    | otherwise             = suitInput ++ toImpute
  where
    suitsOnly = map snd suitInput :: [Suit]
    toImpute = map (\x -> (13, x)) $ findMissingSuits suitsOnly

findMissingSuits :: [Suit] -> [Suit]
findMissingSuits availableSuits =
    [P, K, H, T] \\ availableSuits

orderSuits :: [(Int, Suit)] -> [(Int, Suit)]
orderSuits = sortBy (comparing snd)

solve :: [String] -> String
solve parsedInput
    | length (nub parsedInput) /= length parsedInput = "GRESKA"
    | otherwise                                      =
        unwords . map (show . fst) . orderSuits . imputer $ basicSolve parsedInput