import Data.List (nub)

main :: IO ()
main = do
    input <- getContents
    let preppedInput :: [String]
        preppedInput = tail $ lines input
    let uniqueCities :: [Int]
        uniqueCities = map (length . nub) $ getTestCases preppedInput
    putStrLn . unlines $ map show uniqueCities

getTestCases :: [String] -> [[String]]
getTestCases [] = []
getTestCases (x:xs) = take n xs : getTestCases rest
    where
        n = read x :: Int
        rest = drop (n + 1) (x:xs)