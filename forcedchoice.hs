main :: IO ()
main = do
    input <- getContents
    let preppedInput = map words $ lines input
    let chosenNum = head preppedInput !! 1
    let comparisonLists = map tail $ tail preppedInput
    putStrLn . unlines $ map (keepOrRemove chosenNum) comparisonLists

keepOrRemove :: String -> [String] -> String
keepOrRemove chosen list
    | chosen `elem` list = "KEEP"
    | otherwise = "REMOVE"