main :: IO ()
main = do
    input <- getContents
    let preppedInput = lines input
    let licenceValidity = read (head preppedInput) :: Int
    let validJunkDays = take licenceValidity . map (\x -> read x :: Int) . words $ last preppedInput
    let minJunk = minimum validJunkDays
    let numberedDays = zip [0..] validJunkDays
    print . findEarliestDay $ filter (dayMatcher minJunk) numberedDays

dayMatcher :: Int -> (Int, Int) -> Bool
dayMatcher min_junk valid_days = min_junk == snd valid_days

findEarliestDay :: [(Int, Int)] -> Int
findEarliestDay numbered_days = minimum $ map fst numbered_days