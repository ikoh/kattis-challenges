# Kattis challenges

Contains challenges attempted on [Kattis](https://open.kattis.com/), which is a website containing a list of programming challenges from various competitions. The file names in this project correspond to Kattis’s problem IDs. The problems can be accessed on the Kattis website using the following format for the URL: `open.kattis.com/problems/<problem_id>` (one does not need to be logged in to view them). File names with `*_v*` added after the problem ID indicate alternative solutions.
