input :: String -> [[String]]
input a = map words $ tail $ lines a

string2Float :: String -> Float
string2Float str = read str :: Float

multiplier :: [String] -> Float
multiplier inputList = (head toFloat) * (last toFloat)
    where
        toFloat = map string2Float inputList

solve :: [[String]] -> String
solve a = show $ sum multiplied
    where multiplied = map multiplier a

main :: IO ()
main = interact $ solve . input
