toInt :: [String] -> [Int]
toInt = map (\x -> read x :: Int)

processInput :: String -> [Int]
processInput rawInput = toInt $ lines rawInput

buildSequence :: [Int] -> [Int]
buildSequence input = [lowerBound .. upperBound]
    where
        lowerBound = head input
        upperBound = input !! 1

splitter :: String -> [String]
splitter [] = []
splitter str = [head str] : splitter (tail str)

sumSequenceDigits :: [Int] -> [Int]
sumSequenceDigits = map (sum . toInt . splitter . show)

referenceList :: [Int] -> [Int]
referenceList input = replicate seqLength reference
    where
        reference = last input
        seqLength = (input !! 1) - head input + 1

zipper :: [Int] -> [(Int, Int, Int)]
zipper processedInput = zip3 seq summedDigits ref
    where
        seq = buildSequence processedInput
        summedDigits = sumSequenceDigits seq
        ref = referenceList processedInput

matches :: [(Int, Int, Int)] -> [Int]
matches tupleList = map (\(x, _, _) -> x) $ filter (\(_, x, y) -> x == y) tupleList

getMinMax :: [Int] -> [String]
getMinMax matchList = map show [minimum matchList, maximum matchList]

solve :: [Int] -> [String]
solve = getMinMax . matches . zipper

main :: IO ()
main = do
    input <- getContents
    putStrLn $ unlines $ solve $ processInput input