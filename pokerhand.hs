import Data.List (sort, group)

main :: IO ()
main = do
    input <- getLine
    let preppedInput = map head $ words input
    print . maximum . map length . group $ sort preppedInput