-- This version of the code includes the use of newtypes

import Data.List (group, sort)
import Data.Maybe (mapMaybe)

type FormattedSoloID = String

newtype UncheckedIDInt = UncheckedIDInt Int deriving (Eq, Ord, Show)
newtype SoloID = SoloID Int deriving (Eq, Ord, Show)
newtype IDCount = IDCount Int deriving (Eq, Ord, Show)

main :: IO ()
main = do
    raw <- getContents
    let
        parsedInt :: [[UncheckedIDInt]]
        parsedInt
            = map (map (UncheckedIDInt .  read) . words)
            . filter (\x -> length x > 1)
            $ lines raw
    putStrLn . unlines . formatter . mconcat $ map soloFinder parsedInt

soloFinder :: [UncheckedIDInt] -> [SoloID]  -- [SoloID] to have length 1
soloFinder xs = mapMaybe (getSolo . pairMaker) groupedXs
  where
    groupedXs = group $ sort xs :: [[UncheckedIDInt]]
    pairMaker :: [UncheckedIDInt] -> (UncheckedIDInt, IDCount)
    pairMaker xs = (head xs, IDCount (length xs))

unchecked2SoloID :: UncheckedIDInt -> SoloID
unchecked2SoloID (UncheckedIDInt a) = SoloID a

getSolo :: (UncheckedIDInt, IDCount) -> Maybe SoloID
getSolo (key, IDCount 1) = Just (unchecked2SoloID key)
getSolo (_, _)   = Nothing

formatter :: [SoloID] -> [FormattedSoloID]
formatter xs = map singleFormatter zip2Format
  where
    index = [1 .. length xs] :: [Int]
    zip2Format = zip xs index :: [(SoloID, Int)]
    singleFormatter :: (SoloID, Int) -> FormattedSoloID
    singleFormatter (SoloID id, index) = "Case #" ++ show index ++ ": " ++ show id
