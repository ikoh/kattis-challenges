import Data.List ( foldl' )

getWidth :: [String] -> Int
getWidth rawList = read $ head rawList :: Int

shardArea :: [String] -> Int
shardArea shard = (read $ head shard :: Int) * (read $ last shard :: Int)

getArea :: [String] -> Int
getArea rawList = foldl' (+) 0 $ map (shardArea . words) $ drop 2 rawList

getArea2 :: Int -> [String] -> Int
getArea2 = foldl' (\ accum x -> accum + shardArea (words x))

getHeight :: [String] -> String
getHeight rawList = show $ div area width
    where
        -- area = getArea rawList
        area = getArea2 0 $ drop 2 rawList
        width = getWidth rawList

main :: IO ()
main = do
    input <- getContents
    putStrLn $ getHeight $ lines input

-- Example input for testing: "4\n7\n2 3\n1 4\n1 2\n1 2\n2 2\n2 2\n2 1"

-- This solution fails one of the test cases because it takes too long to
-- complete (the time limit is 6 seconds). Optimisation is needed to read
-- and process lines within that limit (the maximum number of lines to process
-- is 5 million).

-- Note on 2021-04-16: try using Data.Text and see if performance improves!