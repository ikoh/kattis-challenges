main :: IO ()
main = do
    input <- getContents
    let answerKey = tail $ lines input
        comparison = zip answerKey (tail answerKey)
        correct = length $ filter (uncurry (==)) comparison
    print correct