import Data.List

allPermutationsLessOrig :: String -> [String]
allPermutationsLessOrig raw = filter (/= raw) $ nub $ permutations raw

smallestLargerNumber :: String -> String
smallestLargerNumber raw
    | largerVals == [] = "0"
    | otherwise = show $ minimum largerVals
    where
        rawVal = read raw :: Int
        perms = map (\x -> read x :: Int) $ allPermutationsLessOrig raw
        largerVals = filter (> rawVal) perms

solve :: String -> String
solve raw
    | length raw == 1 = "0"
    | length (nub raw) == 1 = "0"
    | otherwise = smallestLargerNumber raw

main :: IO ()
main = do
    input <- getLine
    putStrLn $ solve input