skip2 :: String -> String
skip2 = tail . tail

solve :: String -> String
solve [] = []
solve (x:xs)
    | x `elem` "aeiou" = x : solve (skip2 xs)
    | otherwise = x : solve xs

main :: IO ()
main = do
    input <- getLine
    putStrLn . unwords $ map solve $ words input