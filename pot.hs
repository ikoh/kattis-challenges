input :: String -> [String]
input raw = tail $ lines raw

splitter :: String -> (Integer, Integer)
splitter elem = (firstInt, lastInt)
    where
        firstInt = read $ init elem :: Integer
        lastChar2String = last elem : []
        lastInt = read lastChar2String :: Integer

powerMaker :: (Integer, Integer) -> Integer
powerMaker inputTuple = fst inputTuple ^ (snd inputTuple)

transformer :: String -> Integer
transformer = powerMaker . splitter

solve :: [String] -> String
solve rawList = show $ sum $ map transformer rawList

main :: IO ()
main = interact $ solve . input
