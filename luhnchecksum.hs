import Data.Char (digitToInt)

main :: IO ()
main = do
    raw <- getContents
    let parsed :: [[Int]]
        parsed =  map digitToInt <$> tail (lines raw)
        solve = map (luhnVerifier . luhnAdder . evenDoubler)
    putStrLn . unlines $ solve parsed

evenDoubler :: [Int] -> [Int]
evenDoubler xs = multiplier <$> paired
  where
    paired :: [(Int, Int)]
    paired = zip xs (reverse [1 .. length xs])

    multiplier :: (Int, Int) -> Int
    multiplier (val, index)
        | even index = val * 2
        | otherwise  = val

luhnAdder :: [Int] -> [Int]
luhnAdder xs = singleNumProcessor <$> xs
  where
    singleNumProcessor :: Int -> Int
    singleNumProcessor x
        | x > 9     = separateAndSum x
        | otherwise = x

    separateAndSum :: Int -> Int
    separateAndSum x = sum . map digitToInt $ show x

luhnVerifier :: [Int] -> String
luhnVerifier xs
    | mod (sum xs) 10 == 0 = "PASS"
    | otherwise            = "FAIL"