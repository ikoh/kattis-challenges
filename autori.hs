spacemaker :: Char -> Char
spacemaker '-' = ' '
spacemaker x = x

solve :: String -> String
solve raw = map head $ words $ map spacemaker raw

main :: IO ()
main = do
    input <- getLine
    putStrLn $ solve input