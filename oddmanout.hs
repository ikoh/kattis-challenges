import Data.List (group, sort)
import Data.Maybe (mapMaybe)

type UncheckedID = String
type UncheckedIDInt = Int
type SoloID = UncheckedIDInt
type FormattedSoloID = String
type IDCount = Int

main :: IO ()
main = do
    raw <- getContents 
    let parsed = map words . filter (\x -> length x > 1) $ lines raw :: [[UncheckedID]]
    let parsedInt = map (map read) parsed :: [[Int]]
    putStrLn . unlines . formatter . mconcat $ map soloFinder parsedInt

soloFinder :: [UncheckedIDInt] -> [SoloID]  -- [SoloID] to have length 1
soloFinder xs = mapMaybe (getSolo . pairMaker) groupedXs
  where
    groupedXs = group $ sort xs :: [[UncheckedIDInt]]
    pairMaker :: [UncheckedIDInt] -> (UncheckedIDInt, IDCount)
    pairMaker x = (head x, length x)

getSolo :: (UncheckedIDInt, IDCount) -> Maybe SoloID
getSolo (key, 1) = Just key
getSolo (_, _)   = Nothing

formatter :: [SoloID] -> [FormattedSoloID]
formatter xs = map singleFormatter zip2Format
  where
    index = [1 .. length xs] :: [Int]
    zip2Format = zip xs index :: [(SoloID, Int)]
    singleFormatter :: (SoloID, Int) -> FormattedSoloID
    singleFormatter (id, index) = "Case #" ++ show index ++ ": " ++ show id