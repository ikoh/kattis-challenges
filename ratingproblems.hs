data Direction = Min | Max

main :: IO ()
main = do
    input <- getContents
    let linedInput = lines input
        preamble = stringToInt . words $ head linedInput
        remJudges = head preamble - last preamble
        ratings = stringToInt $ tail linedInput
    putStrLn . unwords . map show $ solve remJudges ratings

stringToInt :: [String] -> [Int]
stringToInt = map (\x -> read x :: Int)

avg :: [Int] -> Double
avg [] = 0.0
avg list = fromIntegral (sum list) / fromIntegral (length list)

impute :: Int -> Direction -> [Int]
impute remainingJudges Min = replicate remainingJudges (-3)
impute remainingJudges Max = replicate remainingJudges 3

solve :: Int -> [Int] -> [Double]
solve 0 ratingsList = replicate 2 $ avg ratingsList
solve nonZeroRemJudges ratingsList = [minVal, maxVal]
    where
        minVal = avg (ratingsList ++ impute nonZeroRemJudges Min)
        maxVal = avg (ratingsList ++ impute nonZeroRemJudges Max)