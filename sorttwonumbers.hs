main :: IO ()
main = do
    input <- getLine
    let processedInput = map (\x -> read x :: Int) $ words input
    putStrLn . toString $ arranger processedInput

arranger :: [Int] -> [Int]
arranger inputList
    | x == y = [x, y]
    | x < y = [x, y]
    | otherwise = [y, x]
    where
        x = head inputList
        y = last inputList

toString :: [Int] -> String
toString inputList = unwords $ map show inputList