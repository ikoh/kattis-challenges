import Data.List (sort)

main :: IO ()
main = do
    input <- getContents
    let preppedInput = map words . init $ lines input
    putStrLn . unlines $ map checker preppedInput 

str2Int :: [String] -> [Integer]
str2Int = map (\x -> read x :: Integer)

checker :: [String] -> String
checker strList
    | lhs == rhs = "right"
    | otherwise  = "wrong"
    where
    intList = sort $ str2Int strList
    lhs = (head intList)^2 + (intList !! 1)^2
    rhs = (last intList)^2