someSum :: String -> String
someSum "1"  = "Either"
someSum "2"  = "Odd"
someSum "3"  = "Either"
someSum "4"  = "Even"
someSum "5"  = "Either"
someSum "6"  = "Odd"
someSum "7"  = "Either"
someSum "8"  = "Even"
someSum "9"  = "Either"
someSum "10" = "Odd"

main :: IO ()
main = do
    input <- getLine
    putStrLn $ someSum input