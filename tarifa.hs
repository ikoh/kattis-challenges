monthlyCap :: [String] -> [Int]
monthlyCap raw = replicate (last toReplicate) (head toReplicate)
    where toReplicate = map (\x -> read x :: Int) $ take 2 raw

monthlyUsage :: [String] -> [Int]
monthlyUsage raw = map (\x -> read x :: Int) $ drop 2 raw

solve :: [String] -> String
solve raw = show available
    where
        leftovers = zipWith (-) (monthlyCap raw) (monthlyUsage raw)
        available = sum $ leftovers ++ [read (head raw) :: Int]

main :: IO ()
main = do
    input <- getContents
    putStrLn $ solve $ lines input