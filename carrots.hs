solve :: String -> String
solve a = last $ words $ head $ lines a

main :: IO ()
main = interact solve