main :: IO ()
main = do
    input <- getLine
    let parsed = words $ map (char2Space ';') input
    print . length . mconcat $ map solve parsed

char2Space :: Char -> Char -> Char
char2Space patt x
    | x == patt = ' '
    | otherwise = x

solve :: String -> [Int]
solve str
    | length str == 1 = [read str :: Int]
    | otherwise       = multipleQns str

multipleQns :: String -> [Int]
multipleQns str = [first .. second]
  where
    strParsed = words $ map (char2Space '-') str
    first = read (head strParsed) :: Int
    second = read (last strParsed) :: Int