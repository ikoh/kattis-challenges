import Data.List ( sort )

buildSequence :: Int -> [Int]
buildSequence n
    | even n = [2 .. (div n 2)]
    | otherwise = [2 .. (div n 2 + 1)]

-- Logic for configurations with same values e.g. 2,2 and 5,5
checkValiditySame :: Int -> [Int] -> [[Int]]
checkValiditySame myInputInt [] = []
checkValiditySame myInputInt (x:xs)
    | mod myInputInt x == 0 = [x, x] : checkValiditySame myInputInt xs
    | otherwise = checkValiditySame myInputInt xs

generateSameConfig :: Int -> [[Int]]
generateSameConfig myInputInt = checkValiditySame myInputInt $ buildSequence myInputInt

-- Logic for configurations with different values e.g. 6,5 and 17,16
checkValidityDifferent :: Int -> [Int] -> [[Int]]
checkValidityDifferent myInputInt [] = []
checkValidityDifferent myInputInt (x:xs)
    | elem remainder [0, x] = [x, x - 1] : checkValidityDifferent myInputInt xs
    | otherwise = checkValidityDifferent myInputInt xs
    where
        summed = sum [x, x - 1]
        remainder = mod myInputInt summed

generateDifferentConfig :: Int -> [[Int]]
generateDifferentConfig myInputInt = checkValidityDifferent myInputInt $ buildSequence myInputInt

toString :: [Int] -> String
toString myListlet = mconcat [show $ head myListlet, ",", show $ last myListlet]

solve :: Int -> [String]
solve myInputInt = header : map toString (sort allConfigs)
    where
        allConfigs = generateSameConfig myInputInt ++ generateDifferentConfig myInputInt
        header = show myInputInt ++ ":"

main :: IO ()
main = do
    input <- getLine
    let inputInt = read input :: Int
    putStrLn . unlines $ solve inputInt
