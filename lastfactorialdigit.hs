main :: IO ()
main = do
    input <- getContents
    let preppedInput :: [String]
        preppedInput = tail $ lines input
    putStrLn . unlines $ map lastDigit preppedInput

lastDigit :: String -> String
lastDigit "1" = "1"
lastDigit "2" = "2"
lastDigit "3" = "6"
lastDigit "4" = "4"
lastDigit _ = "0"